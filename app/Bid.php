<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $fillable = ['product_id', 'user_id', 'bid_price', 'admin_id', 'isConfirmed'];
    protected $appends = ['status', 'statusCode'];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function admin(){
        return $this->belongsTo('App\User', 'admin_id');
    }

    public function getStatusAttribute(){
        return $this->isConfirmed ? 'confirmed' : 'pending';
    }

    public function getStatusCodeAttribute(){
        return $this->isConfirmed ? 'success' : 'danger';
    }
}
