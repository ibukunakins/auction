<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'description', 'user_id', 'costPrice', 'status', 
                            'photo','startDate', 'endDate', 'cat_id'];
    protected $appends = ['isExpired', 'numberOfBids', 'highestBid', 'slug'];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function category()
    {
      return $this->belongsTo('App\Category', 'cat_id');
    }

    public function getEndDateAttribute($value){
        return Carbon::parse($value);
    }

    public function bids()
    {
        return $this->hasMany('App\Bid');
    }

    public function getIsExpiredAttribute()
    {
      return (Carbon::now() > $this->endDate);
    }

    public function getNumberOfBidsAttribute()
    {
        return $this->bids->count();
    }

    public function getHighestBidAttribute()
    {
        if($this->bids->count() < 1){
            return $this->min_bid_price;
        }
        return $this->bids->max('bid_price');
    }

    public function scopeUnexpired($query){
         return $query->where('endDate', '>', now());
    }

    public function getSlugAttribute(){
        return str_slug($this->name);
    }
    
    public function orders(){
        return $this->belongsToMany('App\Order', 'order_product')->withPivot('quantity', 'costPrice', 'profitMargin', 'subAmount')->withTimestamps();
    }
}
