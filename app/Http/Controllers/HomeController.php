<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bid;
use App\Product;
use App\Category;
use Auth;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Home';
        return view('home', compact('title'));
    }

    public function account(Request $request){
        $title = 'My Account';
        $bids = Bid::where('user_id', $request->user()->id)->get();
        return view('pages.account', compact('title', 'bids'));
    }

    public function createProduct()
    {
        if(Auth::user()->userLevel != 'Admin'){
            abort(404);
        }
        $title = 'Create New Product';
       return view('pages.product', compact('title', 'categories'));
    }

    public function products()
    {
        if(Auth::user()->userLevel != 'Admin'){
            abort(404);
        }
        $products = Product::with('category')->paginate(20);
        $title = 'Manage Products';
        return view('pages.products', compact('products', 'title'));
    }


    public function deleteProduct(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->bids()->delete();
        $product->delete();
        return redirect()->back()->with(['message' => 'Product Deleted']);
    }


    public function storeProduct(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'photo' => 'required|image',
            'min_bid_price' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date|after:yesterday',
        ]);
        $product = new Product ($request->only(['name', 'description', 'min_bid_price', 'status','startDate', 'endDate', 'cat_id']));
        //dd($product);
        $fileName = $request->file('photo')->store('public/uploads');
        $fileName = explode('/', $fileName);
        $product->photo = $fileName[2];
        $request->user()->products()->save($product);
        return redirect()->route('createProduct')->with(['message' => 'Product created successfully']);
    }

    public function users()
    {
        $users = User::where('userLevel', 1)->with('bids')->paginate();
        $title = 'All Registered bidders';
        return view('pages.users', compact('users', 'title'));
    }

    public function deleteUser(Request $request)
    {
        $user = User::find($request->user_id);
        $user->bids()->delete();
        $user->delete();
        return redirect()->back()->with(['message' => 'User Deleted']);   
    }
}
