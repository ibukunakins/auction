<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Cache;
use Auth;
class AuctionController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth')->only('create', 'edit', 'store');
    }

    public function index(Request $request)
    {
        $category_id = $request->category_id;

        if($category_id > 0){
            $products = Product::unexpired()->where('cat_id', $category_id)->with('category', 'bids')->orderBy('endDate', 'asc')->paginate(15);
            $categories = Cache::get('categories');
            $categoryName = $categories[$category_id - 1]->name;
            $title = 'Auction Category - '.$categoryName;
        }else{
            $products = Product::unexpired()->with('category', 'bids')->orderBy('endDate', 'asc')->paginate(15);
            $title = 'All Auction Products';
            $category_id = 0;
        }
        return view('pages.auctions', compact('title', 'products', 'category_id'));
    }

    public function show($id){
        $product = Product::find($id);
        $title = $product->name;
        return view('pages.auction', compact('title', 'product'));
    }

    public function create()
    {
      $title = 'Create Auction Product';
      return view('pages.create', ['title' => $title]);
    }

    public function store(Request $request)
    {
        $auction = new Auction($request->all());
        $auction->save();
        return redirect()->route('auction.create')->withMessage('Product created Successfully');
    }

    public function edit(Request $request, $id)
    {
        // code...
    }

    public function search(Request $request)
    {
        $q = $request->input('q');
        $products = Product::where('name', 'LIKE', '%'. $q .'%')
            ->orWhere('id', 'LIKE', '%'. $q .'%')->paginate(15);
        $products->appends(['q' => $q]);
        $category_id = 0;
        $title = 'Search Result for '.$q; 
        return view('pages.auctions', compact('products', 'title', 'category_id'));
    }

   

}
