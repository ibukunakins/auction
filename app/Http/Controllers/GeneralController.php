<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderProduct;


class GeneralController extends Controller
{
    public function index(){
        $invest = OrderProduct::with('product', 'order')->get();
        return $invest->toJson();
    }
    
    public function show($id){
        $orderProduct = OrderProduct::find($id);
        return $orderProduct;
    }
}
