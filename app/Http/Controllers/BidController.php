<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bid;
use Auth;

class BidController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->userLevel != 'Admin'){
            abort(404);
        }
		$bids = Bid::orderBy('created_at', 'desc')->paginate(20);
		$title = 'All Submitted Bids';
		return view('pages.bids', compact('bids', 'title'));    	
    }

    public function store(Request $request)
    {
       /* if(Auth::user()->userLevel != 'Admin'){
            abort(404);
        }*/
    	$request->validate([
    		'product_id' => 'required|integer',
    		'bid_price' => 'required|integer'
    	]);
    	$bid = new Bid($request->only(['product_id', 'bid_price']));
    	$bid->admin_id = 0;
    	$bid->user_id = Auth::user()->id;
    	$bid->isConfirmed = 0;
    	$bid->save();
    	return redirect()->back()->with('success', 'Bid Submitted Successfully');
    }

    public function confirm(Request $request){
        if(Auth::user()->userLevel != 'Admin'){
            abort(404);
        }
    	$bid = Bid::find($request->bid_id);
    	$bid->isConfirmed = 1;
    	$bid->save();
    	return redirect()->back()->with('message', 'Bid was Confirmed Successfully');
    }
}
