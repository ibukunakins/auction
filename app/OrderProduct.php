<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderProduct extends Pivot
{
    protected $fillable = ['order_id', 'quantity', 'costPrice', 'profitMargin', 'subAmount', 'product_id'];
    
    public function order(){
        return $this->belongsTo('App\Order');
    }
    
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
