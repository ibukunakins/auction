<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'firstName', 'lastName', 'email', 'password', 'userLevel', 'photo'
    ];
    protected $appends = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function Bids()
    {
        return $this->hasMany('App\Bid');
    }

    public function ApprovedBids(){
        return $this->hasMany('App\Bid', 'admin_id');
    }

    public function getNameAttribute()
    {
        return $this->firstName.' '.$this->lastName;
    }

    public function getUserLevelAttribute($value)
    {
        $accessLevel = ['1' => 'User', '4' => 'Admin'];
        return $accessLevel[$value];
    }
}
