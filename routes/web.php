<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('testing', 'GeneralController@index');
Route::view('/', 'pages.welcome', ['title' => 'Home'])->name('index');
Route::view('/about', 'pages.about', ['title' => 'About'])->name('about');
Route::view('/instructions', 'pages.faq', ['title' => 'Instructions'])->name('faqs');
Route::view('/contact', 'pages.contact', ['title' => 'Contact Page'])->name('contact');
Route::get('auctions', 'AuctionController@index')->name('auctions');
Route::get('auction/create', 'AuctionController@create')->name('auction.create');
Route::get('auction/search', 'AuctionController@search')->name('auction.search');
Route::get('auction/{id}', 'AuctionController@show')->name('auction.show');
Route::get('product/create', 'HomeController@createProduct')->name('createProduct');
Route::get('products', 'HomeController@products')->name('productList');
Route::post('product/store', 'HomeController@storeProduct')->name('storeProduct');
Route::post('product/delete', 'HomeController@deleteProduct')->name('deleteProduct');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account', 'HomeController@account')->name('account');
Route::post('profileUpdate', 'HomeController@profileUpdate')->name('profileUpdate');
Route::get('bids', 'BidController@index')->name('bids');
Route::post('bids/confirm', 'BidController@confirm')->name('bidConfirm');
Route::post('bid/store', 'BidController@store')->name('bidStore');
Route::get('users', 'HomeController@users')->name('users');
Route::post('user/delete', 'HomeController@deleteUser')->name('userDelete');
// RouteS
 //- home page
 // - registration form
 // login form
 // auctions page
 // auctions category page
 // product page
 // add product
 // user profile
