<?php

use Illuminate\Database\Seeder;
use App\Product;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = factory(App\Order::class, 20)->create();
        $products = Product::orderBy('id')->get(['id', 'costPrice']);
        $orders->each(function (App\Order $order) use ($products) {
            $order->products()->attach(
            $products->random(rand(2, 3))->pluck('id')->toArray(),
            ['costPrice' => rand(9999, 999999) , 'quantity' => rand(2, 5), 'profitMargin' => rand(5, 10), 'subAmount' => rand(99999, 999999)]
                );
            });
    }
}
