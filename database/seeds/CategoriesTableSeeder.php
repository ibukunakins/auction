<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Cars']);
        Category::create(['name' => 'Furniture']);
        Category::create(['name' => 'Trucks']);
        Category::create(['name' => 'Electronics']);
        Category::create(['name' => 'Textiles']);
        Category::create(['name' => 'Jewellery']);
        Category::create(['name' => 'Footwear']);
        Category::create(['name' => 'Ornaments']);
    }
}
