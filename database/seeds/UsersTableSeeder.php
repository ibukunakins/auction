<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'ibukunakins',
            'firstName' => 'Authentic',
            'lastName' => 'One',
            'email' => 'ibukunakins@gmail.com',
            'userLevel' => 4,
            'password' => bcrypt('davdel05')
        ]);
        User::create([
            'username' => 'umarbawa',
            'firstName' => 'Umar',
            'lastName' => 'Bawa',
            'email' => 'umarbawa30@gmail.com',
            'userLevel' => 4,
            'password' => bcrypt('vvvvenlo')
        ]);
        factory(App\User::class, 50)->create();
    }
}
