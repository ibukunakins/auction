<?php

use Faker\Generator as Faker;

$factory->define(App\Bid::class, function (Faker $faker) {
    $productCount = 20;
    $userCount = 50;
    return [
        'product_id' => rand(1, $productCount),
        'user_id' => rand(3, $userCount),
        'bid_price' => $faker->randomFloat(2, 9999, 99999),
        'admin_id' => rand(1, 2),
        'isConfirmed' => rand(0, 1),
        
    ];
});
