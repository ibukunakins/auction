<?php

use Faker\Generator as Faker;


$factory->define(App\Product::class, function (Faker $faker) {
    $catSize = 8;
    return [
      'name' => $faker->company,
      'description' => $faker->paragraph(2),
      'costPrice' => $faker->randomFloat(2, 999, 9999),
      'status' => rand(1,2),
      'photo' => $faker->imageUrl(160, 160),
      'startDate' => $faker->dateTimeBetween('-30 days'),
      'endDate' => $faker->dateTimeBetween('-30 days', '+30 days'),
      'cat_id' => rand(1, $catSize),
      'user_id' => rand(1, 2)
    ];
});
