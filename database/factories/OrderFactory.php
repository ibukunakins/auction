<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'user_id' => rand(1, 20),
        'numberOfProducts' => rand(1, 3),
        'totalAmount' => $faker->randomFloat(2, 99999, 999999)
    ];
});
