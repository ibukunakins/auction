<div class="col-md-3">
  <div class="card">
      <div class="card-header">{{ __('User Menu') }}</div>

      <div class="card-body">
          @guest
          <form method="POST" action="{{ route('login') }}">
            <div class="form-group">
              <label for="email">Email address</label>
              <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
            </div>
           {!! csrf_field() !!}
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" name="remember" id="remember">
              <label class="form-check-label" for="remember">Remember Me</label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          @endguest
           @auth

           @if(Auth::user()->userLevel == 'Admin')
           <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ url('account') }}">My Account</a></li>
              <li class="list-group-item"><a href="{{ url('bids') }}">View Bids</a></li>
              <li class="list-group-item"><a href="{{ url('product/create') }}">Add Product</a></li>
              <li class="list-group-item"><a href="{{ url('products') }}">Manage Products</a></li>
              <li class="list-group-item"><a href="{{ url('users') }}">Manage Users</a></li>
              <li class="list-group-item"><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a></li>

            </ul>
            @else
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ url('account') }}">My Account</a></li>
              <li class="list-group-item"><a href="{{ url('auctions') }}">Auctions</a></li>
              <li class="list-group-item"><a href="{{ url('instructions') }}">Help</a></li>
              <li class="list-group-item"><a href="{{ url('contact') }}">Contact</a></li>
              <li class="list-group-item"><a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a></li>

            </ul>

            @endif
            @endauth
      </div>
  </div>

</div>
