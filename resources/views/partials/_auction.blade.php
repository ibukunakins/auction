<div class="col-md-4">
  <div class="card">
      <img class="card-img-top" src="{{ asset('storage/uploads/'.$product->photo) }}" alt="{{ $product->name }}">
      <div class="card-body">
        <h5 class="card-title"><a href="{{ url('auction/'.$product->id)}}">{{ $product->name}}</a></h5>
        <p class="card-text">{{substr($product->description, 0, 75)}} ...</p>
        <table class="table table-condensed table-striped">
          <tbody>
            <tr>
              <th>Current Bid</th>
              <td>{{ $product->highestBid }}</td>
            </tr>
            <tr>
              <th>Bids</th>
              <td>{{ $product->numberOfBids }}</td>
            </tr>
            <tr>
              <th>{{ $product->isExpired ? 'Expired' : 'Expires' }}</th>
              <td>{{ $product->endDate->diffForHumans() }}</td>
            </tr>
          </tbody>
        </table>
      </div>

    <div class="card-footer">
      <a href="{{ url('auction/'.$product->id )}}" class="btn btn-sm btn-primary">Read more...</a>
    </div>
  </div>
</div>
