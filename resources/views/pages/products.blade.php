@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                <div class="card">
                  <div class="card-header">
                    Registered Bidders
                  </div>
                  <div class="card-body">
                    @include('partials._errors')
                    <table class="table-condensed table-hover table">
                        <tr>
                           <th>S/N</th>
                           <th>Date Added</th>
                           <th>Name</th>
                           <th>Minimum Bid</th>
                           <th>Current Bid</th>
                           <th>Bids</th>
                           <th>Action</th>
                         </tr> 
                         @foreach($products as $i => $product)
                            <tr>
                              <td>{{ $i+1 }}</td>
                              <td>{{ $product->created_at->format('d M, Y') }}</td>
                              <td>{{ $product->name }}</td>
                              <td>{{ $product->min_bid_price }}</td>
                              <td>{{ $product->highestBid }}</td>
                              <td>{{ $product->numberOfBids }}</td>
                              <td>
                                <form method="post" action="{{ url('product/delete') }}">
                                  <input type="hidden" name="product_id" value="{{ $product->id}}" />
                                  @csrf
                                  <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete {{ $product->name}} ?')">delete</button>
                              </form>
                             
                            </td>
                            </tr>
                         @endforeach
                    </table>
                    {!! $products->links() !!}
                  </div>
                </div>
              </div> <!-- /column -->
          </div>  <!-- /row -->
        </div> <!-- /container -->
@endsection
@section('styles')
<style>
.tab-content > .active{
  margin: 30px 0;
}
</style>
@endsection