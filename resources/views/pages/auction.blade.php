@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                 @include('partials._errors')
                <div class="container">

                        <div class="row">
                                <div class="col-md-6">
                                        <img src="{{ asset('storage/uploads/'.$product->photo)}}" class="img-fluid" alt="{{ $product->name}}" width="600" height="600" />
                                        @auth
                                            @if (Auth::user()->userLevel == 'Admin')
                                                    <a href="{{ url('edit/'.$product->id)}}">Edit Product</a>
                                            @endif
                                        @endauth
                                        
                                </div>
                                <div class="col-md-6">
                                        <h4>Product Details</h4>
                                        <p class="lead">{{ $product->description}} </p>
                                        <p />
                                        <table class="table table-condensed table-striped">
                  <tbody>
                    <tr>
                      <th>Current Bid</th>
                      <td align="right">{{ $product->highestBid }}</td>
                    </tr>
                    <tr>
                      <th>Bids</th>
                      <td align="right">{{ $product->numberOfBids }}</td>
                    </tr>
                    <tr>
                      <th>{{ $product->isExpired ? 'Expired' : 'Expires' }}</th>
                      <td align="right">{{ $product->endDate->diffForHumans() }}</td>
                    </tr>
                    <tr>
                      <th></th>
                      <td align="right">
                             @if($product->isExpired)
                                     Bidding on this product has ended
                             @else
                                     @auth
                                      <form action="{{ url('bid/store') }}" method="post">
                                          <div class="form-group">
                                                  <label for="bid_price">You must bid at least {{ $product->highestBid+50}}</label>
                                                  <input type="hidden" name="product_id" value="{{ $product->id}}">
                                                  <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                      <span class="input-group-text" id="basic-addon3">NGN</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="bid_price" name="bid_price" aria-describedby="basic-addon3">
                                                </div>
                                                @csrf
                                                <button type="submit" class="btn btn-warning btn-sm">Place my Bid</button>
                                          </div>

                                      </form>
                                      @endauth
                                      @guest
                                        Please <a href="{{ url('register') }}">Register</a> or <a href="{{ url('login')}}">Sign in </a>to place a bid
                                      @endguest
                                @endif
                        </td>
                    </tr>
                  </tbody>
                </table>
                                </div>

                        </div>
                </div>
              </div>
          </div>
        </div>
@endsection
