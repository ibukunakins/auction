@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
      @include('partials._sidebar')
      <div class="col-md-9">
            <div class="card">
                  <div class="card-header">
                    Login/Registration Page
                  </div>
                  <div class="card-body">
                    <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Register</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Login</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#references" role="tab" data-toggle="tab">Reset Password</a>
                      </li>
                    </ul>

<!-- Tab panes -->
<div class="tab-content">
<div role="tabpanel" class="tab-pane fade in active" id="profile">...</div>
<div role="tabpanel" class="tab-pane fade" id="buzz">bbb</div>
<div role="tabpanel" class="tab-pane fade" id="references">ccc</div>
</div>
                  </div>
            </div>
      </div>
  </div>
</div>


@endsection
