@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
      @include('partials._sidebar')
        <div class="col-md-9">
            <div class="card">
                  <div class="card-header">
                  Contact Page
                  </div>
                  <div class="card-body">
                        For more information on bidding and products, please contact the following numbers 08148000549 and 09023003595
                  </div>
            </div>
      </div>
  </div>
</div>

@endsection
