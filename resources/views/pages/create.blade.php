@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
      @include('partials._sidebar')
      <div class="col-md-9">
            <div class="card">
                  <div class="card-header">
                    Create Product
                  </div>
                  <div class="card-body">
                     @include('partials._errors')
                    <form method="post" action="{{ url('auction/store')}}">
                      {{ csrf_field() }}
                        <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Product Name" value="{{ old('name')}}">
                        </div>
                        <div class="form-group">
                          <label for="description">Description</label>
                          <textarea class="form-control" name="description"></textarea>
                        </div>
                        <div class="form-group">
                          <label>Product Image </label>
                          <div class="custom-file">
                          <input type="file" class="custom-file-input" id="customFile">
                          <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                        </div>
                        <div class="form-group">
                              <select class="form-control" required name="category_id">
                                <option value="">Select Category</option>
                                @foreach ($dataCache['categories'] as $category)
                                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                              </select>
                        </div>
                        <div class="form-group">
                          <label>Minimum Bid Price</label>
                          <input type="number" class="form-control" name="min_bid_price" id="min_bid_price" placeholder="0.00">
                        </div>
                        <div class="form-group">
                          <label>Start Date</label>
                          <input type="date" class="form-control" name="startDate" id="startDate" placeholder="mm/dd/YYYY">
                        </div>
                        <div class="form-group">
                          <label>End Date</label>
                          <input type="date" class="form-control" name="endDate" id="endDate" placeholder="mm/dd/YYYY">
                        </div>
                          <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                  </div>
            </div>
      </div>
  </div>
</div>


@endsection
