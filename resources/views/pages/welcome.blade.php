@extends('layouts.app')

@section('content')

<div class="container">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="{{ url('img/slide-1.jpg') }}" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>Welcome to Nigeria Auctions.</h1>
                <p>A system where buying and selling of goods take place by placing bids and taking the bids then selling to the highest bidder.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="{{ url('img/slide-2.jpg') }}" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Register and Bid.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="third-slide" src="{{ url('img/slide-3.jpg') }}" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>Bid Online and Get Items at Cheap Prices.</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
              </div>
            </div>
          </div>
        </div>
        
      </div>
</div>
    <div style="min-height: 30px"></div>

         <div class="container">
           <!-- Example row of columns -->
           <div class="row">
             <div class="col-md-4">
               <h3>Who We Are</h3>
               <p>We are Nigeria's Auction, one of Nigeria's biggest auction sites.</p>
               <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
             </div>
             <div class="col-md-4">
               <h2>What We Do</h2>
               <p>We place auction goods for interested users to place bids on interested goods and we accept the bids and sell to the highest bidder  </p>
               <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
             </div>
             <div class="col-md-4">
               <h2>Our Services</h2>
               <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
               <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
             </div>
           </div>

           <hr>

         </div> <!-- /container -->
@endsection
