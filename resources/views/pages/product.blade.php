@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			  @include('partials._sidebar')
			  <div class="col-md-9">
			  		<h3>{{ $title }}</h3>
			  		<div class="card">
			  			<div class="card-header">
			  				Add New Product
			  			</div>
			  			<div class="card-body">
			  				@include('partials._errors')
			  				<form method="POST" enctype="multipart/form-data" action="{{ route('storeProduct') }}">
			  					{{ csrf_field() }}
			  					<div class="form-group row">
                              		<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

	                              	<div class="col-md-6">
	                                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

	                                  @if ($errors->has('name'))
	                                      <span class="invalid-feedback" role="alert">
	                                          <strong>{{ $errors->first('name') }}</strong>
	                                      </span>
	                                  @endif
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

	                              	<div class="col-md-6">
	                                 <textarea name="description" id="description" class="form-control" required></textarea>
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="product-image" class="col-md-4 col-form-label text-md-right">{{ __('Product Image') }}</label>

	                              	<div class="col-md-6">
	                                 	<input type="file" class="form-control" name="photo" />
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="category_id" class="col-md-4 col-form-label text-md-right">{{ __('Product Category') }}</label>

	                              	<div class="col-md-6">
	                                 	<select name="cat_id" required class="form-control">
											<option value="">Select Category</option>
											@foreach($dataCache['categories'] as $c)
												<option value="{{ $c->id }}">{{ $c->name }}</option>
											@endforeach
	                                 	</select>
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="min_bid_price" class="col-md-4 col-form-label text-md-right">{{ __('Minimum Bid Price') }}</label>

	                              	<div class="col-md-6">
	                                 	<input id="min_bid_price" type="text" class="form-control{{ $errors->has('min_bid_price') ? ' is-invalid' : '' }}" name="min_bid_price" value="{{ old('min_bid_price') }}" required placeholder="0.00">

	                                  @if ($errors->has('min_bid_price'))
	                                      <span class="invalid-feedback" role="alert">
	                                          <strong>{{ $errors->first('min_bid_price') }}</strong>
	                                      </span>
	                                  @endif
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="start_date" class="col-md-4 col-form-label text-md-right">{{ __('Start date') }}</label>

	                              	<div class="col-md-6">
	                                 	<input id="start_date" type="text" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}" name="startDate" value="{{ old('start_date') }}" required placeholder="YYYY-MM-DD">

	                                  @if ($errors->has('start_date'))
	                                      <span class="invalid-feedback" role="alert">
	                                          <strong>{{ $errors->first('startDate') }}</strong>
	                                      </span>
	                                  @endif
	                              	</div>
                         		</div>
                         		<div class="form-group row">
                              		<label for="end_date" class="col-md-4 col-form-label text-md-right">{{ __('End Date') }}</label>

	                              	<div class="col-md-6">
	                                 	<input id="end_date" type="text" class="form-control{{ $errors->has('endDate') ? ' is-invalid' : '' }}" name="endDate" value="{{ old('end_date') }}" required placeholder="YYYY-MM-DD">

	                                  @if ($errors->has('end_date'))
	                                      <span class="invalid-feedback" role="alert">
	                                          <strong>{{ $errors->first('endDate') }}</strong>
	                                      </span>
	                                  @endif
	                              	</div>
                         		</div>
                         		<div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Register') }}
                                  </button>
                              </div>
                          </div>

			  				</form>
			  			</div>
			  		</div>
			  </div>
		</div><!--/row-->
	</div><!--/container-->
@endsection