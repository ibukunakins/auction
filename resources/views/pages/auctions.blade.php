@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                <div class="container">
                        <form method="get" action="">
                                <div class="form-row">
                                        <div class="form-group">
                                                   <select class="form-control" required name="category_id">
                                                     <option value="">Select Category</option>
                                                     @foreach ($dataCache['categories'] as $category)
                                                           <option value="{{ $category->id }}" {{ $category->id == $category_id ? 'selected' : ''}}>{{ $category->name }}</option>
                                                     @endforeach
                                                     <option value="0" {{ $category_id == 0 ? 'selected' : ''}}>All</option>
                                                   </select>
                                        </div>
                                        <div class="form-group col-offset-1">
                                                <button type="submit" class="btn btn-info">Apply</button>
                                        </div>
                                </div>
                        </form>
                        <div class="row">
                                @each('partials._auction', $products, 'product')
                                {!! $products->links() !!}
                        </div>
                </div>
              </div>
          </div>
        </div>
@endsection
