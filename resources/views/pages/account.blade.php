@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                <div class="card">
                  <div class="card-header">
                    My Account
                  </div>
                  <div class="card-body">
                    @include('partials._errors')
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">View Profile</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Edit Profile</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">My Bids</a>
                      </li>
                    </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                      <table class="table table-hover">
                          <tr>
                            <td>Name</td>
                            <th>{{ Auth::user()->firstName.' '.Auth::user()->lastName }}</th>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <th>{{ Auth::user()->email }}</th>
                          </tr>
                          <tr>
                            <td>Username</td>
                            <th>{{ Auth::user()->username }}</th>
                          </tr>
                          <tr>
                            <td>Access Level</td>
                            <th>{{ Auth::user()->userLevel }}</th>
                          </tr>
                      </table>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                          <form method="POST" action="{{ route('profileUpdate') }}" aria-label="{{ __('Register') }}">
                          @csrf

                          <div class="form-group row">
                              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                              <div class="col-md-6">
                                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                  @if ($errors->has('name'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="username" class="col-md-4 col-form-label text-md-right">Username</label>

                              <div class="col-md-6">
                                  <input id="name" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ Auth::user()->username }}" required autofocus>

                                  @if ($errors->has('username'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('username') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                              <div class="col-md-6">
                                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required>

                                  @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                              <div class="col-md-6">
                                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group row">
                              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                              <div class="col-md-6">
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>

                          <div class="form-group row mb-0">
                              <div class="col-md-6 offset-md-4">
                                  <button type="submit" class="btn btn-primary">
                                      {{ __('Register') }}
                                  </button>
                              </div>
                          </div>
                      </form> <!-- End of Profile form -->
                      
                    </div><!-- /end of tab -->
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                      @if($bids->count() > 0)
                        <div class="table-responsive">
                          <table class="table table-borderless table-stripe">
                            <tr>
                              <th>Date</th>
                              <th>Product</th>
                              <th>Bid Price</th>
                              <th>Status</th>
                            </tr>
                            @foreach($bids as $bid)
                              <tr>
                                <td>{{ $bid->created_at-> toDayDateTimeString() }}</td>
                                <td>{{ $bid->product->name }}</td>
                                <td>{{ $bid->bid_price }}</td>
                                <td>{{ $bid->status }}</td>
                              </tr>
                            @endforeach
                          </table>
                        </div>
                        @else
                          <div class="alert alert-info">
                            You have not submitted any bids
                          </div>
                        @endif
                    </div> <!-- /end of tab -->
                  </div>
                  </div>
                </div>
              </div> <!-- /column -->
          </div>  <!-- /row -->
        </div> <!-- /container -->
@endsection
@section('styles')
<style>
.tab-content > .active{
  margin: 30px 0;
}
</style>
@endsection