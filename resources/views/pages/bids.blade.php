@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                <div class="card">
                  <div class="card-header">
                    Submitted Bids
                  </div>
                  <div class="card-body">
                    @include('partials._errors')
                    <table class="table-condensed table-hover table">
                        <tr>
                           <th>S/N</th>
                           <th>Date</th>
                           <th>Item Description</th>
                           <th>Bidder Name</th>
                           <th>Bid Price (&#8358;)</th>
                           <th>Status</th>
                           <th>Action</th>
                         </tr> 
                         @foreach($bids as $i => $bid)
                            <tr>
                              <td>{{ $i+1 }}</td>
                              <td>{{ $bid->created_at->format('d M, Y h:i:s') }}</td>
                              <td>{{ $bid->product->name }}</td>
                              <td>{{ $bid->user->name }}</td>
                              <td>{{ number_format($bid->bid_price, 2, '.', ', ') }}</td>
                              <td><button class="btn btn-sm btn-{{ $bid->statusCode }}">{{ $bid->status }}</button></td>
                              <td>@if(!$bid->isConfirmed)
                                <form method="post" action="{{ url('bids/confirm') }}">
                                  <input type="hidden" name="bid_id" value="{{ $bid->id}}" />
                                  @csrf
                                  <button type="submit" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to confirm this Bid?')">confirm</button>
                              </form>
                              @endif
                            </td>
                            </tr>
                         @endforeach
                    </table>
                    {!! $bids->links() !!}
                  </div>
                </div>
              </div> <!-- /column -->
          </div>  <!-- /row -->
        </div> <!-- /container -->
@endsection
@section('styles')
<style>
.tab-content > .active{
  margin: 30px 0;
}
</style>
@endsection