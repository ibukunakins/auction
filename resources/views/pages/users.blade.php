@extends('layouts.app')
@section('content')
        <div class="container">

          <div class="row">
              @include('partials._sidebar')

              <div class="col-md-9">
                <h3> {{ $title }} </h3>
                <div class="card">
                  <div class="card-header">
                    Registered Bidders
                  </div>
                  <div class="card-body">
                    @include('partials._errors')
                    <table class="table-condensed table-hover table">
                        <tr>
                           <th>S/N</th>
                           <th>Date Registered</th>
                           <th>Username</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Number of Bids</th>
                           <th>Action</th>
                         </tr> 
                         @foreach($users as $i => $user)
                            <tr>
                              <td>{{ $i+1 }}</td>
                              <td>{{ $user->created_at->format('d M, Y') }}</td>
                              <td>{{ $user->username }}</td>
                              <td>{{ $user->name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $user->bids->count() }}</td>
                              <td>
                                <form method="post" action="{{ url('user/delete') }}">
                                  <input type="hidden" name="user_id" value="{{ $user->id}}" />
                                  @csrf
                                  <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete {{ $user->name}} ?')">delete</button>
                              </form>
                             
                            </td>
                            </tr>
                         @endforeach
                    </table>
                    {!! $users->links() !!}
                  </div>
                </div>
              </div> <!-- /column -->
          </div>  <!-- /row -->
        </div> <!-- /container -->
@endsection
@section('styles')
<style>
.tab-content > .active{
  margin: 30px 0;
}
</style>
@endsection