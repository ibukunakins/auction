@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">

      @include('partials._sidebar')
      <div class="col-md-9">
            <div class="card">
                  <div class="card-header">
                    Auctions Guide and Instructions
                  </div>
                  <div class="card-body">
                        All users are advised to follow simple steps for bidding and registrations whereas visitors can only view products without having the access to bid on them until they register.
                  </div>
            </div>
      </div>
  </div>
</div>

@endsection
